from distutils.core import setup

setup(
    name="pypex",
    version="1.0.0",
    packages=[""],
    scripts=["bin/pypex"],
    url="",
    license="Copyright 2014 Trey Brisbane",
    author="Trey Brisbane",
    author_email="tbrisbane@gmail.com",
    description="PypEx - A Python-based Text/Stream Manipulation Tool"
)
