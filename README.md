# PypEx - A Python-based Text/Stream Manipulation Tool
## Description
PypEx is a Python-based text/stream manipulation tool, similar to sed and based on pyp.

## Author
Trey Brisbane

## Usage Instructions
Run PypEx as follows:

`pypex "<command>" << <input file>`

Or, alternatively (on Unix):

`cat <input file> | pypex "<command>"`